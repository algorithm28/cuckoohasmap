/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rattanalak.cuckoo;

/**
 *
 * @author Rattanalak
 */
public class Cuckoo {

    private int key;
    private String value;
    private Cuckoo[] Table1 = new Cuckoo[99];
    private Cuckoo[] Table2 = new Cuckoo[99];
    private int length1 = Table1.length;
    private int length2 = Table2.length;
    private int index;

    public Cuckoo(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public Cuckoo() {

    }

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public int hasing1(int key) {
        return key % length1;
    }

    public int hasing2(int key) {
        return (key / 99) % length2;
    }

    public void add(int key, String value) {
        if (Table1[hasing1(key)] != null && Table1[hasing1(key)].key == key) {
            Table1[hasing1(key)].key = key;
            Table1[hasing1(key)].value = value;
            return;
        }
        if (Table2[hasing2(key)] != null && Table2[hasing2(key)].key == key) {
            Table2[hasing2(key)].key = key;
            Table2[hasing2(key)].value = value;
            return;
        }
        index = 0;
        checkCuckoo(index, key, value);

    }

    private void checkCuckoo(int index, int key1, String value1) {
        while (true) {
            if (index == 0) {
                if (Table1[hasing1(key1)] == null) {
                    Table1[hasing1(key1)] = new Cuckoo();
                    Table1[hasing1(key1)].key = key1;
                    Table1[hasing1(key1)].value = value1;
                    return;
                }
            } else {
                if (Table2[hasing2(key1)] == null) {
                    Table2[hasing2(key1)] = new Cuckoo();
                    Table2[hasing2(key1)].key = key1;
                    Table2[hasing2(key1)].value = value1;
                    return;
                }
            }
            if (index == 0) {
                String dropValue = Table1[hasing1(key1)].value;
                int tempKey = Table1[hasing1(key1)].key;
                Table1[hasing1(key1)].key = key1;
                Table1[hasing1(key1)].value = value1;
                key1 = tempKey;
                value1 = dropValue;
            } else {
                String dropValue = Table2[hasing2(key1)].value;
                int dropKey = Table2[hasing2(key1)].key;
                Table2[hasing2(key1)].key = key1;
                Table2[hasing2(key1)].value = value1;
                key1 = dropKey;
                value1 = dropValue;
            }
            index = (index + 1) % 2;
        }
    }

    public Cuckoo get(int key) {
        if (Table1[hasing1(key)] != null && Table1[hasing1(key)].key == key) {
            return Table1[hasing1(key)];
        }
        if (Table2[hasing2(key)] != null && Table2[hasing2(key)].key == key) {
            return Table2[hasing2(key)];
        }
        return null;
    }

    @Override
    public String toString() {
        return "Cuckoo{" + "key=" + key + ", value=" + value + '}';
    }
         public Cuckoo delete(int key) {
        if (Table1[hasing1(key)] != null && Table1[hasing1(key)].key == key) {
            Cuckoo temp = Table1[hasing1(key)];
            Table1[hasing1(key)] = null;
            return temp;
        }
        if (Table2[hasing2(key)] != null && Table2[hasing2(key)].key == key) {
            Cuckoo temp = Table2[hasing2(key)];
           Table2[hasing2(key)] = null;
            return temp;
        }
        return null;
    }

}
