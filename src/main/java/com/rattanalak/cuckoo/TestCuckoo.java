/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.rattanalak.cuckoo;

/**
 *
 * @author Rattanalak
 */
public class TestCuckoo {

    public static void main(String[] args) {
        Cuckoo cuckoo = new Cuckoo();
        cuckoo.add(101, "Plai");
        cuckoo.add(1, "Rattanalak");
        cuckoo.add(102, "63166079");
        cuckoo.add(202, "41360");
         System.out.println(cuckoo.get(101).toString());
        System.out.println(cuckoo.get(1).toString());
        System.out.println(cuckoo.get(102).toString());
        System.out.println(cuckoo.get(202).toString());
        System.out.println("-----------------------------------------------------");
        System.out.println("Delete 102");
        System.out.println(cuckoo.get(102));
        cuckoo.delete(102);
        System.out.println(cuckoo.get(102));

    }
}
